{-# LANGUAGE OverloadedStrings #-}

module Main where

import Options.Applicative
import Data.Semigroup ((<>))
import qualified Data.Text as T
import qualified Data.Text.IO as TIO


import Data.Markdown
import Data.Markdown.Parse (parseMarkdown)
import Data.Markdown.Html (toHtmlAsText)


main :: IO ()
main = do
  -- parse terminal arguments
  opts <- execParser $
    info (optionsParser <**> helper)
      ( briefDesc
     <> progDesc "Convert a markdown file into an Html file"
      )
  -- convert
  let inFile = optInFile opts
  let outFile = optOutFile opts `orElse` inFile ++ ".html"
  markdown <- TIO.readFile inFile
  let html = toHtmlAsText $ parseMarkdown markdown
  TIO.writeFile outFile html


-- hint: FilePath = String
data Options = Options
  { optInFile  :: FilePath
  , optOutFile :: Maybe FilePath -- optional
  }

optionsParser :: Parser Options
optionsParser =
        Options
    <$> argument str
        ( metavar "FILE"
       <> help "File to parse containing markdown"
        )
    <*>( optional $ strOption
        ( long "output"
       <> short 'o'
       <> metavar "FILE"
       <> help "File to write converted Html into"
        )
       )

(orElse) maybeVal alternative =
  case maybeVal of
    (Just v) -> v
    Nothing -> alternative


