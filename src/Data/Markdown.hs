module Data.Markdown where

import qualified Data.Text as T

-- | Representation of a restricted Markdown
-- using as guideline the
-- <http://spec.commonmark.org/0.27/ CommonMark Specification>.
-- A markdown document is a list of markdown blocks.
newtype MarkdownDoc = MarkdownDoc [Block] deriving Show

-- | An isolated part of a Markdown document
-- like a header, a paragraph or a list.
-- This representation is restricted to
-- "leafblocks", so nested blocks like a header inside a quote  or
-- nested lists are not supported.
data Block
  = Header Level StyledText
  | Paragraph StyledText
  | UList [StyledText]
  | Quote StyledText
  | BlankLine
  deriving (Show)

type Level = Int

-- | Text with differently styled parts called chunks.
type StyledText = [TxtChunk]
-- | Styled chunks may enclose multiple, differently
-- styled chunks themselves.
data TxtChunk
  = Bold [TxtChunk]
  | Italic [TxtChunk]
  | Normal T.Text
  deriving (Show)

