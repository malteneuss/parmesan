-- Change default type of "abc" to T.Text instead of String
{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE ApplicativeDo #-}

module Data.Markdown.Parse where


import Control.Monad (void)
import Control.Monad.State as S
import Data.Monoid ((<>))
import Control.Applicative --((<|>),(<*>), (<$>), some, many)
import Data.Char (isAlpha, isSpace)
-- Fast string representation
import qualified Data.Text as T

import Text.Megaparsec.Combinator --(count', count, someTill)
import Text.Megaparsec
import Text.Megaparsec.Prim
import qualified Text.Megaparsec.Char as C
-- parser default input-stream-type becomes ‘T.Text’
import Text.Megaparsec.Text

import Data.Markdown


-- Parser modifiers:
-- optional  -- zero or one, or regex "?"
-- many      -- zero or more, or regex "*"
-- some      -- one or more, or regex "+"
-- (<*)      -- parse the left and keep its result,
--              then parse the right and ignore its result
-- (*>)      -- parse the left and ignore its result,
--              then parse the right and keep its result
--  Everything else is well explained in
--  <https://jakewheat.github.io/intro_to_parsing/>


-- Info:
-- A value of type "Parser a" is a thing that can parse a String
-- to a value of type "a".
-- Basically everything in this file is a parser.

parseMarkdown :: T.Text -> MarkdownDoc
parseMarkdown s = parseToEOF markdownDoc (s `T.append` "\n\n") `orIfFailed`
  MarkdownDoc [Paragraph [Normal s]]

-- | To eliminate many eof corner cases, the string/file/stream of
-- markdown has to end on "\n\n", which matches a blankLines.
markdownDoc :: Parser MarkdownDoc
markdownDoc = MarkdownDoc <$> manyTill block eof
  where block :: Parser Block
        block = choice $ map try allBlockParsers

-- paragraphBlock must be last to be a catch-it-all
allBlockParsers = [ headerBlock, uListBlock, quoteBlock, paragraphBlock]

---- block parsers ----

-- Must start at line where header begins
headerBlock :: Parser Block
headerBlock = do
  -- ignore  leading spaces
  -- after 4 spaces it's not a header but a code block
  void $ count' 0 3 oneLineSpaceChar
  hashes <- count' 1 6 $ char '#'
  -- after hashes must come at least one space
  oneLineSpaceChar
  txt <- line
  optional blankLines
  let level = length hashes
  let parsedTxt =
       (parseToEOF styledTxt txt) `orIfFailed` [Normal txt]
  return $ Header level parsedTxt


quoteBlockStart :: Parser ()
quoteBlockStart = do
  -- ignore  leading spaces
  -- after 4 spaces it's not a quoteBlock but a code block
  void $ count' 0 3 oneLineSpaceChar
  void $ string ">"
  -- no need for space after '>'

quoteBlock :: Parser Block
quoteBlock = do
  quoteBlockStart
  txtlines <- line `sepBy1` quoteBlockStart
  optional blankLines
  let txt = T.concat txtlines
  let parsedTxt :: StyledText
      parsedTxt =
       (parseToEOF styledTxt txt) `orIfFailed` [Normal txt]
  return $ Quote parsedTxt


paragraphBlock :: Parser Block
paragraphBlock = do
  lines <- paragraphLines
  let txt = T.concat lines
  let parsedTxt =
       (parseToEOF styledTxt txt) `orIfFailed` [Normal txt]
  return $ Paragraph parsedTxt


startOfUListBlock :: Parser ()
startOfUListBlock = do
  -- after 4 spaces it's not a UList but a code block
  void $ count' 0 3 oneLineSpaceChar
  void $ char '*'
  -- after '*' must come a space and a nonspaceChar
  void oneLineSpaceChar

uListBlock :: Parser Block
uListBlock = do
  startOfUListBlock
  items <- line `sepBy1` startOfUListBlock
  optional blankLines
  let parsedItems = map parselocal items
  return $ UList parsedItems
  where parselocal txt = (parseToEOF styledTxt txt) `orIfFailed` [Normal txt]


---- styled text parsers ----

styledTxt :: Parser StyledText
-- styledTxt = some $ choice [try boldTxt, try italicTxt, normalTxt]
styledTxt = some $ (Normal . T.pack) <$> (some anyChar) --choice [try boldTxt, try italicTxt, normalTxt]

-- Match a bold text chunk.
-- No support for nested styled text.
-- In any case consumes leading "**".
boldTxt :: Parser TxtChunk
boldTxt = dbg "bold" $ do
  -- First char after opening "**" must not be space.
  (string "**" >> lookAhead nonSpaceChar)
  -- look for shortest closing "**"
  enclosed <- manyTill anyChar $ string "**"
  return $ Bold [ Normal $ T.pack enclosed ]

-- | Match an italic text chunk.
-- No support for nested styled text.
-- In any case consumes leading "*".
italicTxt :: Parser TxtChunk
italicTxt = dbg "ita" $ do
  -- First char after opening "*" must not be space.
  (string "*" >> lookAhead nonSpaceChar)
  -- look for shortest closing "*"
  enclosed <- manyTill anyChar $ string "*"
  return $ Italic [ Normal $ T.pack enclosed ]

normalTxt :: Parser TxtChunk
normalTxt = dbg "norm" $ do
  option "" $ string "*" -- workaround to continue parsing malformed italic txt
  option "" $ string "*" -- workaround to continue parsing malformed bold txt
  -- parse until the next chunk begins
  txt <- someTill anyChar $ choice [ eof, void $ try boldTxt, void $ try italicTxt]
         -- eof is important to parse to the end if there is no subsequent styled chunk
  return . Normal $ T.pack txt



-- Matches "", " ", " \n"
blankLine :: Parser T.Text
blankLine = dbg "bLn" $ T.append <$> (T.pack <$> many oneLineSpaceChar) <*> nlOrEof

-- A block of of some blank lines (at least one!)
blankLines :: Parser T.Text
blankLines = dbg "bLnBlock" $ do
  firstLine <- blankLine -- also matches ""
  rest <- (("" <$ eof) <|> try blankLines) -- needed to avoid matching "" endlessly
  return $ T.append firstLine rest
  -- where startOfOtherBlock = choice

-- Must contain at least a single nonSpaceChar
line :: Parser T.Text
line = dbg "ln" $ do
  spaces <- T.pack <$> many oneLineSpaceChar
  nonspace <- T.singleton <$> nonSpaceChar -- at least one nonSpaceChar
  rest <- T.pack <$> many oneLineChar
  endOfLine <- nlOrEof
  return $ T.concat [spaces, nonspace, rest, endOfLine]

-- For easier debugging
paragraphLines :: Parser [T.Text]
paragraphLines = dbg "para" $ do
  lines <- someTill line startOfOtherBlock
  return lines
  where startOfOtherBlock = choice
          [ void $ try blankLines -- several blank lines that paragraph also consumes
          , void $ try $ lookAhead quoteBlock
          , void $ try $ lookAhead headerBlock
          , void $ try $ lookAhead uListBlock
          , eof
          ]



----- little helper parsers ----

oneLineChar :: Parser Char
oneLineChar = noneOf ['\n'] <?> "anyChar that is not newline"

oneLineSpaceChar :: Parser Char
oneLineSpaceChar = satisfy (\c -> isSpace c && c /= '\n')
  <?> "any spaceChar that is not newline"

nonSpaceChar :: Parser Char
nonSpaceChar = satisfy (\c -> not $ isSpace c) <?> "anyChar that is no spaceChar"

nl :: Parser T.Text
nl = T.singleton <$> char '\n'

nlOrEof :: Parser T.Text
nlOrEof = (nl <|> ("" <$ eof))
-- nl = T.pack <$> eol

---- little parse result helpers
(orIfFailed) parseResult alternativeValue =
  either (const alternativeValue) id parseResult

parseToEOF parser streamToParse = parse (parser <* eof) "" streamToParse

-- only for testing
pt :: (Show a) => Parser a -> T.Text -> IO ()
pt = parseTest
p parser streamToParse = parse parser "" streamToParse
