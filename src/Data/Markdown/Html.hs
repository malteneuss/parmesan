{-# LANGUAGE OverloadedStrings #-}

module Data.Markdown.Html where

import Control.Monad (forM_)
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL

import Text.Blaze.Html5 as H
import Text.Blaze.Html5.Attributes as A
import Text.Blaze.Html.Renderer.Text (renderHtml)

import Data.Markdown

toHtmlAsText :: MarkdownDoc -> T.Text
toHtmlAsText doc = TL.toStrict . renderHtml $ mdToHtml doc

mdToHtml :: MarkdownDoc -> Html
mdToHtml (MarkdownDoc blocks) = docTypeHtml $ do
  H.head $ do
    H.title "Converted Markdown"
  H.body $ do
    forM_ blocks blockToHtml


blockToHtml :: Block -> Html
blockToHtml BlankLine = toHtml (" \n" :: T.Text)
blockToHtml (Paragraph txt) = p $ styledTxtToHtml txt
blockToHtml (Quote txt) = blockquote $ styledTxtToHtml txt
blockToHtml (UList txts) = ul $ forM_ txts styledTxtToHtml
blockToHtml (Header level txt) =
  headerTags !! level $ styledTxtToHtml txt


styledTxtToHtml :: StyledText -> Html
styledTxtToHtml chunks = forM_ chunks txtChunkToHtml

txtChunkToHtml :: TxtChunk -> Html
txtChunkToHtml (Bold nestedChunks) = strong $ styledTxtToHtml nestedChunks
txtChunkToHtml (Italic nestedChunks) = em $ styledTxtToHtml nestedChunks
txtChunkToHtml (Normal rawtxt) = toHtml rawtxt

-- | List index corresponds to header level
headerTags = [undefined, h1, h2, h3, h4, h5, h6]
